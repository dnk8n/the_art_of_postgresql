#!/usr/bin/env bash

cat << EOF > ${PGDATA}/pg_hba.conf
local   all     all                     trust
host    all     all     0.0.0.0/0	md5

EOF
unzip /volume/TheArtOfPostgreSQLdatabase.zip -d /tmp
cd /tmp/TheArtOfPostgreSQL
make
rm -rf /tmp/TheArtOfPostgreSQL
