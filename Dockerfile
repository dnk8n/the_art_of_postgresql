FROM postgres:10.23-bullseye
ENV DEBIAN_FRONTEND noninteractive
WORKDIR /var/lib/postgresql
COPY --chown=postgres:postgres .psqlrc .psqlrc
RUN apt-get update && apt-get install -y make pgloader postgresql-10-ip4r \
        postgresql-10-hll unzip vim &&\
    mkdir -p .psql && touch .psql/.psql_history-postgres &&\
        chown postgres:postgres -R .psql &&\
    mkdir /volume && chown postgres:postgres /volume
USER postgres:postgres
