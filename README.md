### Notes:

- The dump is large, have at least 15-20GB of disk storage available
- Move your downloaded copy of the dump archive provided to `./volume_data/TheArtOfPostgreSQLdatabase.zip`
- If SELinux is enabled on your system, you might need to run `chcon -Rv -t container_file_t ./volume_data`
- Run `docker-compose up -d --build` (ommit the `--build` flag if already built)
- Useful to run `docker-compose logs -f` to see log output of initial run
- docker-compose exec postgres-taop psql -U taop (files are mounted at `xyz`)