-- name: top-artists-by-album
-- Get the list of the N artists with the most albums
select
  artist.name,
  count(*) as albums
from
  artist
  left join album using(artistid)
group by
  artist.name
order by
  albums desc
limit
  : n;

-- name: list-albums-by-artist
-- List the album titles and duration of a given artist
select
  album.title as album,
  sum(milliseconds) * interval '1 ms' as duration
from
  album
  join artist using(artistid)
  left join track using(albumid)
where
  artist.name = : name
group by
  album
order by
  album;


-- name: genre-top-n
-- Get the N top tracks by genre
select
  genre.name as genre,
  case when length(ss.name) > 15 then substring(
    ss.name
    from
      1 for 15
  ) || ' … ' else ss.name end as track,
  artist.name as artist
from
  genre
  left join lateral
  /*
  * the lateral left join implements a nested loop over
  * the genres and allows to fetch our Top-N tracks per
  * genre, applying the order by desc limit n clause.
  *
  * here we choose to weight the tracks by how many
  * times they appear in a playlist, so we join against
  * the playlisttrack table and count appearances.
  */
  (
    select
      track.name,
      track.albumid,
      count(playlistid)
    from
      track
      left join playlisttrack using (trackid)
    where
      track.genreid = genre.genreid
    group by
      track.trackid
    order by
      count desc
    limit
      : n
  )
  /*
  * the join happens in the subquery's where clause, so
  * we don't need to add another one at the outer join
  * level, hence the "on true" spelling.
  */
  ss(name, albumid, count) on true
  join album using(albumid)
  join artist using(artistid)
order by
  genre.name,
  ss.count desc;
